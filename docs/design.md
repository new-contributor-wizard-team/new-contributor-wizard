### How To Design

Designing of GUI for this application is explained in [this](https://blog.shanky.xyz/gsoc-2018-week-1-and-2.html) blog.

##### Sign Up Screen

![Sign Up Screen](https://blog.shanky.xyz/images/NewContributorWizardSignUp.png)

##### Sign In Screen

![Sign In Screen](https://blog.shanky.xyz/images/NewContributorWizardSignIn.png)

##### Dashboard

![Dashboard](https://blog.shanky.xyz/images/NewContributorWizardDashboard.png)
